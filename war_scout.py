#!/usr/bin/env python3
from subprocess import Popen, PIPE
import sys
import os

def run(cmd, logfile):
    with open(logfile+".out","w") as out, open(logfile+".err","w") as err:
        p = Popen(cmd, universal_newlines=True, stdout=out, stderr=err)
        p.wait()
    return p



def main(ip_source):
    base_cmd = ["sipvicious_svwar", "-D", "-m", "INVITE"]

    targets = open(ip_source, "r")
    for target in targets:
        enumerate_cmd = base_cmd.copy()
        enumerate_cmd.append("udp://"+target.strip())
        # print(enumerate_cmd)
        run(enumerate_cmd, target[:target.find(":")])
    targets.close()


if __name__ == "__main__":
    if (len(sys.argv) != 2):
        print("Usage:\n\tpython3 " + sys.argv[0] + " target_ips.txt")
        print("Traget format is following: ip:port")
        print("i.e:")
        print("\t8.8.8.8:5060")
        print("\t10.0.0.1:5061")
    elif (os.path.isfile(sys.argv[1])):
        main(sys.argv[1])
    else:
        print("ERROR: Can't find input file at path '" + sys.argv[1] + "'")

